# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added

### Changed


## [0.2.1] - 2016-09-16
### Added
- Add SQVideoUploadDelegate method allVideosUploadedSuccessfully

### Changed
- Update README code examples

## [0.2.0] - 2016-09-08
### Added
- SQVideoUploadDelegate protocol to notify video upload complete or error
- Added this CHANGELOG
- Added documentation for the most important functions and classes

### Changed
- Delete temp files after video is done uploading (this prevents the app size from growing)
- Fix upload progress timer not starting

## [0.1.9] - 2016-09-08
### Changed
- Fix error where file path of exported video would not be set correctly

## [0.1.8] - 2016-09-07
### Changed
- This should be considered the initial version
