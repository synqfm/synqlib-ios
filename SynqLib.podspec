#
# Be sure to run `pod lib lint SynqLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SynqLib'
  s.version          = '0.2.1'
  s.summary          = 'SynqLib is a simple Objective-C library that enables upload of videos to the SYNQ platform'
  s.description      = <<-DESC
This library was created to make it easy to integrate SYNQ video uploading into your app.
Please note: this pod is just an add-on to the SYNQ API and is of no use unless you already have created a service for accessing the API, either directly or by using one of our SDKs. (http://docs.synq.fm)
                       DESC

  s.homepage         = 'https://bitbucket.org/synqfm/synqlib-ios.git'
  s.social_media_url = 'http://twitter.com/synqfm'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Kjartan Vestvik' => 'kjartan@synq.fm' }
  s.source           = { :git => 'https://bitbucket.org/synqfm/synqlib-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'SynqLib/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SynqLib' => ['SynqLib/Assets/*.png']
  # }

  s.public_header_files = 'SynqLib/Classes/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'AFNetworking', '~> 3.0'

end
