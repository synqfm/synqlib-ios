//
//  SQAppDelegate.h
//  SynqLib
//
//  Created by Kjartan on 08/31/2016.
//  Copyright (c) 2016 Kjartan. All rights reserved.
//

@import UIKit;

@interface SQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
