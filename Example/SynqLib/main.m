//
//  main.m
//  SynqLib
//
//  Created by Kjartan on 08/31/2016.
//  Copyright (c) 2016 Kjartan. All rights reserved.
//

@import UIKit;
#import "SQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SQAppDelegate class]));
    }
}
